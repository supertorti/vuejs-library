import * as Components from './Components'
import * as Plugins from './Plugins'

export default {

    /**
     * @param Vue {{ component, use }}
     */
    install (Vue) {

        let installed = []

        // Install all components
        for (let ComponentName in Components){
            let Component = Components[ ComponentName ]

            let response = {
                "Type": 'Component',
                "Name": Component.name,
                "Message": 'success'
            }

            try {
                Vue.component(Component.name, Component)
            }
            catch (e) {
                response.Message = 'Failed'
                console.error(e)
            }


            installed.push(response)
        }

        // Install all Plugins
        for (let Plugin in Plugins){

            let response = {
                "Type": 'Plugin',
                "Name": Plugin,
                "Message": 'success'
            }

            try {
                Vue.use(Plugins[ Plugin ])
            }
            catch (e) {
                response.Message = 'Failed'
                console.error(e)
            }


            installed.push(response)
        }


        if(process.env.NODE_ENV === 'development') {
            console.group('%c Install VueJS-Library', 'background-color: green; color: white')
            console.table(installed)
            console.groupEnd()
        }
    }
}


