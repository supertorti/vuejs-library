import BlockUI from "./BlockUI";

export default {

    name: 'BlockUI-Plugin',
    Vue: {},
    mounted:false,
    instance: {},
    css: {},

    install (Vue) {

        this.Vue = Vue

        Vue.blockui = this

        Object.defineProperties(Vue.prototype, {
            $blockui: {
                get () {
                    return Vue.blockui
                }
            }
        })
    },

    mountIfNotMounted (options) {

        options = Object.assign({}, options, this.css)

        let node = document.createElement('div')
        let body = document.querySelector('body')
            body . appendChild(node);

        // noinspection JSCheckFunctionSignatures
        let blockUI = new ( this.Vue.extend(BlockUI) )({
            propsData: options
        })

        blockUI.$mount(node)

        this.instance = blockUI
        this.mounted  = true
    },

    open (options) {

        this.mountIfNotMounted(options)
    },


    close () {
        if (this.mounted === true) {

            let elem = this.instance.$el
            this.instance.$destroy()
            this.instance.$off()
            elem.remove()
            this.mounted = false
        }
    },

    setCSS (cssObj) {
        this.css = {
            css: cssObj
        }
    }
}