/**
 * Exports all plugins from theire folders
 */

import ProgresBarPlugin from './ProgressBar'
import BlockUI from "./BlockUI"

// External import to use Auto-Register
import VueSweetalert2 from "vue-sweetalert2"
import VTooltip from 'v-tooltip'

// noinspection ES6CheckImport
import VueCookies from 'vue-cookies'

export { ProgresBarPlugin, VueSweetalert2, VTooltip, VueCookies, BlockUI }
